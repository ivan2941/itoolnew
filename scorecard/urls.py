# -*- coding: utf-8 -*-
from django.urls import path
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import logout_then_login

from scorecard.views.index import index
from scorecard.views.productivity import productivity
from scorecard.views.penetration import penetration
from scorecard.views.six import six
from scorecard.views.assortment import assortment

app_name = 'scorecard'
urlpatterns = [
    path('', index, name='index'),
    path('productivity/', productivity, name='productivity'),
    path('penetration/', penetration, name='penetration'),
    path('six/', six, name='six'),
    path('assortment/', assortment, name='assortment'),
]