# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response, redirect, render, get_object_or_404
from django.template import RequestContext
from django.http import *
from django.urls import reverse, reverse_lazy
from django.core.exceptions import SuspiciousOperation
from django.views import generic
from django.shortcuts import get_object_or_404

import json

def penetration(request):
    context={}
    context['app']           = 'scorecard'
    context['app_name']      = 'Scorecard'
    context['seccion']       = '%s_penetration' % (context['app'])
    context['logos']         = True
    context['navbar']        = True
    context['menu']          = True
    context['regresar']      = True
    context['url_regresar']  = reverse('scorecard:index')
    context['botones_nav']   = True
    context['titulo']        = 'Penetration'
    context['subtitulo']     = 'Selecciona una opción para filtrar'
    context['footer']        = True

    return render(request, 'scorecard/penetration.html', context)