**iTool New Design**
Proyecto alterno a iTool para el cambio de imagen de la plataforma.

*Antes de empezar asegúrate de contar con los siguientes requerimientos:*

## Crear un ambiente virtual
1. Crea una carpeta raíz donde albergarás tu ambiente virtual.
2. Abre la terminal (cmd + Spacebar, escribe Terminal en el cuadro de texto que aparece y presiona Enter).
3. Dentro de la terminal dirígete a la carpeta raíz que creaste:
    $ cd tu_carpeta/
4. Una vez ubicado dentro de esta carpeta escribe el siguiente comando:
    $ virtualenv -p python3 env_itoolnew
5. Ingresa al ambiente virtual:
    $ cd env_itoolnew/
6. Instala Django:
    $ ./bin/pip install django
7. Clona el repositorio:
    $ git clone https://ivan2941@bitbucket.org/ivan2941/itoolnew.git